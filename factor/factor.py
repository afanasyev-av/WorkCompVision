def Factor(n):
    Ans = []
    d = 2
    while d * d <= n:
        if n % d == 0:
            Ans.append(d)
            n //= d
        else:
            d += 1
    if n > 1:
        Ans.append(n)
    return Ans

n = input("Введи число: ")
for i in range(int(n)):
    print("простые множители числа ", i+1, " = ", Factor(i+1))
