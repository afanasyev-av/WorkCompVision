import unittest

def repack(old_list):
    new_list = []
    first = 0
    last = len(old_list) - 1
    
    while len(new_list) < len(old_list):
        new_list.append(old_list[first])
        if last!=first:
            new_list.append(old_list[last])
        first += 1
        last -= 1
    return new_list

class TestStringMethods(unittest.TestCase):
    def test_equal1(self):
        self.assertEqual(repack([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]), [1, 10, 2, 9, 3, 8, 4, 7, 5, 6])
    def test_equal2(self):
        self.assertEqual(repack([1, 2, 3, 4, 5, 6, 7, 8, 9]), [1, 9, 2, 8, 3, 7, 4, 6, 5])

if __name__ == '__main__':
    unittest.main()
